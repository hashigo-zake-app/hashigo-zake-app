#!/usr/bin/perl 

# A mod_perl module that handles the registration requests from the app

#    Copyright (C) 2012 Robin Sheat
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 HashigoGCMHandler - mod_perl module that handles Google Cloud Messaging
register/unregister requests

This module responds to the URL parts 'register' and 'unregister', with the
'regId' paramter and stores/removes the registration ID from a database.

=cut

use strict;
use warnings;

use Apache2::Connection;
use Apache2::Const;
use Apache2::Log;
use Apache2::RequestRec;
use Data::Dumper;
use DBI;

package HashigoGCMHandler;

sub handler {
    my ($r) = @_;

    my $ua     = $r->headers_in->{'User-Agent'} || 'unknown-agent';
    my $uri    = $r->uri;
    my $args   = $r->args;
    my $config = load_config($r, $ENV{'HZ_CONFIG'} );

    return Apache2::Const::DECLINED if ( !$args );
    if ( $uri =~ m(/register) ) {
        return register( $config, $r );
    }
    if ( $uri =~ m(/unregister) ) {
        unregister( $config, $r);
        return Apache2::Const::OK;
    }
    return Apache2::Const::DECLINED;
}

sub register {
    my ( $config, $req ) = @_;

    my $arg_str = $req->args;
    my $args = parse_args($arg_str);
    my $reg_id = $args->{regId};
    my $dbh = DBI->connect(
        $config->{dbi_data_source},
        $config->{dbi_username},
        $config->{dbi_password},
        { AutoCommit => 1, RaiseError => 1, PrintError => 0 }
    );

    if (!$reg_id) {
       $req->log_error("HashigoGCMHandler: args were provided but didn't include regId: ");
       return Apache2::Const::SERVER_ERROR;
    }
    # Check to see if this regid is already in the database. If it is,
    # just make sure it's defined as registered.
    my $select = $dbh->prepare('SELECT COUNT(*) FROM gcm_regids WHERE regid=?');
    $select->execute($reg_id);
    my $count = $select->fetchrow_arrayref;
    if ($count->[0] != 0) {
        my $update = $dbh->prepare('UPDATE gcm_regids SET registered=1 WHERE regid=?');
        $update->execute($reg_id);
    } else {
        my $insert = $dbh->prepare('INSERT INTO gcm_regids (regid, registered, timestamp) VALUES (?, 1, now())');
        $insert->execute($reg_id);
    }
    return Apache2::Const::OK;
}

sub unregister {
    my ( $config, $req) = @_;

    my $dbh = DBI->connect(
        $config->{dbi_data_source},
        $config->{dbi_username},
        $config->{dbi_password},
        { AutoCommit => 1, RaiseError => 1, PrintError => 0 }
    );
    my $arg_str = $req->args;
    my $args = parse_args($arg_str);
    my $reg_id = $args->{regId};
    if (!$reg_id) {
       $req->log_error("HashigoGCMHandler: args were provided but didn't include regId: $arg_str");
       return Apache2::Const::SERVER_ERROR;
    }
    my $update = $dbh->prepare('UPDATE gcm_regids SET registered=0 WHERE regid=?');
    $update->execute($reg_id);
    return Apache2::Const::OK;
}

# Parses the URL arguments. This does a fairly primitive string manipulation.
sub parse_args {
    my ($str) = @_;

    my @args_parts = split(/&/, $str);
    my %res;
    foreach my $a (@args_parts) {
        my ($k, $v) = split(/=/, $a);
        $res{$k} = $v;
    }
    return \%res;
}

# This loads the configuration file into a hash.
#
# Usage:
#   $config = load_config($config_file);
#
my $config_cache;

sub load_config {
    my ($req, $file) = @_;

    if (!$file) {
        die("No file defined. Try setting the HZ_CONFIG envvar.");
    }
    #TODO do periodic date checks and refresh the config if needed
    return $config_cache if ($config_cache);
    open my $fh, '<', $file or die "Unable to open $file: $!\n";
    my $opts;
    while (<$fh>) {
        s/^(.*?)#/$1/;
        my ( $o, $v ) = m/^(.*?)=(.*)$/;
        next if !$o || !$v;
        $opts->{$o} = $v;
    }
    $config_cache = $opts;
    return $opts;
}
1;
