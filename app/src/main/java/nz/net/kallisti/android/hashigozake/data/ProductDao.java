package nz.net.kallisti.android.hashigozake.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;
import java.util.UUID;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

@Dao
public abstract class ProductDao {
    @Query("SELECT * FROM hashigoproduct WHERE type = 'tap' AND pouringStatus IN ('Now', 'Next', 'NEWEST')")
    public abstract LiveData<List<HashigoProduct>> getTaps();

    @Query("SELECT * FROM hashigoproduct WHERE type = 'bottle' AND pouringStatus IN ('Now')")
    public abstract LiveData<List<HashigoProduct>> getBottles();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<HashigoProduct> products);

    @Query("UPDATE hashigoproduct SET pouringStatus = 'No' WHERE type = :type AND (version != :currentVersion OR version IS NULL)")
    public abstract void removePreviousVersion(String type, String currentVersion);

    @Transaction
    void insertLatestByType(String type, List<HashigoProduct> products) {
        String version = UUID.randomUUID().toString();
        for (HashigoProduct product: products) {
            product.type = type;
            product.version = version;
        }
        insertAll(products);
        removePreviousVersion(type, version);
    }

    @Query("SELECT * FROM hashigoproduct WHERE name = :name AND brewery = :brewery")
    public abstract LiveData<List<HashigoProduct>> getProduct(String name, String brewery);
}
