package nz.net.kallisti.android.hashigozake;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.ProductRepository;
import nz.net.kallisti.android.hashigozake.data.Resource;
import nz.net.kallisti.android.hashigozake.data.Status;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.ui.BottleListAdapter;


public class BottleListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ExpandableListView.OnChildClickListener {
    public static final String TAG = BottleListFragment.class.getSimpleName();
    private static final String SORT_ORDER_KEY = "SORT_ORDER_KEY";

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.list)
    ExpandableListView list;

    private BottleListAdapter adapter;
    private BottleViewModel viewModel;

    public static BottleListFragment newInstance() {
        return new BottleListFragment();
    }

    public BottleListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        adapter = new BottleListAdapter(getActivity());
        final ProductRepository productRepository = ((App) getActivity().getApplication()).getProductRepository();
        viewModel = ViewModelProviders.of(this).get(BottleViewModel.class);
        viewModel.init(productRepository);
        viewModel.getBottles().observe(this, new Observer<Resource<List<HashigoProduct>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<HashigoProduct>> hashigoProducts) {
                if (hashigoProducts == null) {
                    return;
                }
                if (hashigoProducts.status == Status.LOADING) {
                    if (!refreshLayout.isRefreshing()) {
                        progress.setVisibility(View.VISIBLE);
                    }
                } else {
                    progress.setVisibility(View.GONE);
                    refreshLayout.setRefreshing(false);
                }
                if (hashigoProducts.data != null) {
                    int sortOrder = PreferenceManager.getDefaultSharedPreferences(getActivity())
                            .getInt(SORT_ORDER_KEY, BottleListAdapter.SortBy.BREWERY.ordinal());
                    adapter.updateAll(hashigoProducts.data, BottleListAdapter.SortBy.values()[sortOrder]);
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_bottle_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bottle_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        list.setAdapter((ExpandableListAdapter) adapter);
        list.setOnChildClickListener(this);
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                onRefresh();
                return true;
            case R.id.menu_sort_brewery:
                setSort(BottleListAdapter.SortBy.BREWERY);
                return true;
            case R.id.menu_sort_style:
                setSort(BottleListAdapter.SortBy.STYLE);
                return true;
            case R.id.menu_sort_name:
                setSort(BottleListAdapter.SortBy.ALPHABETICAL);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        viewModel.refresh();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        HashigoProduct product = (HashigoProduct) adapter.getChild(groupPosition, childPosition);
        if (product != null) {
            Intent intent = new Intent(getActivity(), ProductActivity.class);
            intent.putExtra(ProductActivity.TITLE, "Beer");
            intent.putExtra(ProductActivity.PRODUCT, product);
            startActivity(intent);
            return true;
        }
        return false;
    }

    private void setSort(BottleListAdapter.SortBy sortBy) {
        if (getActivity() == null) {
            return;
        }
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putInt(SORT_ORDER_KEY, sortBy.ordinal())
                .apply();
        adapter.setCurrentSort(sortBy);
    }
}
