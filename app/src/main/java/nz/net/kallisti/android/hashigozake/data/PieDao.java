package nz.net.kallisti.android.hashigozake.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;
import java.util.UUID;

import nz.net.kallisti.android.hashigozake.data.pies.Pie;

@Dao
public abstract class PieDao {
    @Query("SELECT * FROM pie WHERE available = 'Yes'")
    public abstract LiveData<List<Pie>> getAvailable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<Pie> pies);

    @Query("UPDATE pie SET available = 'No' WHERE version != :currentVersion OR version IS NULL")
    public abstract void removePreviousVersion(String currentVersion);

    @Transaction
    void insertLatest(List<Pie> products) {
        String version = UUID.randomUUID().toString();
        for (Pie pie: products) {
            pie.version = version;
        }
        insertAll(products);
        removePreviousVersion(version);
    }
}
