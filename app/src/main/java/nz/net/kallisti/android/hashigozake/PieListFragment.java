package nz.net.kallisti.android.hashigozake;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.PieRepository;
import nz.net.kallisti.android.hashigozake.data.Resource;
import nz.net.kallisti.android.hashigozake.data.Status;
import nz.net.kallisti.android.hashigozake.data.pies.Pie;
import nz.net.kallisti.android.hashigozake.ui.PieListAdapter;


public class PieListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = PieListFragment.class.getSimpleName();

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.list)
    RecyclerView list;

    private PieListAdapter adapter;
    private PiesViewModel viewModel;

    public static Fragment newInstance() {
        return new PieListFragment();
    }

    public PieListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        adapter = new PieListAdapter();
        final PieRepository pieRepository = ((App) getActivity().getApplication()).getPieRepository();
        viewModel = ViewModelProviders.of(this).get(PiesViewModel.class);
        viewModel.init(pieRepository);
        viewModel.getAvailablePies().observe(this, new Observer<Resource<List<Pie>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<Pie>> pies) {
                if (pies == null) {
                    return;
                }
                if (pies.status == Status.LOADING) {
                    if (!refreshLayout.isRefreshing()) {
                        progress.setVisibility(View.VISIBLE);
                    }
                } else {
                    progress.setVisibility(View.GONE);
                    refreshLayout.setRefreshing(false);
                }
                if (pies.data != null) {
                    adapter.updateAll(pies.data);
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pie_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter);
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                onRefresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        viewModel.refresh();
    }
}
