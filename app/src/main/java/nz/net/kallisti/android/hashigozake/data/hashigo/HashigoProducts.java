package nz.net.kallisti.android.hashigozake.data.hashigo;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This represents the products data from Hashigo Zake
 *
 * @author robin
 */
@Root(strict = false)
public class HashigoProducts implements Serializable {
    private static final long serialVersionUID = -6902765997530249608L;

    private static final String DEBUG_TAG = "HashigoProducts";

    @Attribute(name = "Timestamp", required = false)
    private String timestamp;

    @ElementList(name = "Beers")
    private List<HashigoProduct> products = new ArrayList<>();

    @Element(name = "On", required = false)
    @Path("LastChange")
    private String lastOn;

    @Element(name = "Off", required = false)
    @Path("LastChange")
    private String lastOff;

    /**
     * Get the timestamp of this data, e.g. "16:29 on 05/08/2012"
     *
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the timestamp for this data, in the same form as
     * {@link #getTimestamp()}
     *
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Provides a list of all the products
     *
     * @return the products
     */
    public List<HashigoProduct> getProducts() {
        return products;
    }


    /**
     * Provides the products that last came on. It is in the form brewery+" "+products.
     *
     * @return the last products on
     */
    public String getLastOn() {
        return lastOn;
    }

    public boolean isLatestProduct(HashigoProduct product) {
        return getLastOn() != null && getLastOn().equals(product.toString());
    }
}
