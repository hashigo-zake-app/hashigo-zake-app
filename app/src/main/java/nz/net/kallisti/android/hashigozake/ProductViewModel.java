package nz.net.kallisti.android.hashigozake;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import nz.net.kallisti.android.hashigozake.data.ProductRepository;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

class ProductName {
    String name;
    String brewery;

    public ProductName(String name, String brewery) {
        this.name = name;
        this.brewery = brewery;
    }
}

public class ProductViewModel extends ViewModel {
    private static final String TAG = ProductViewModel.class.getSimpleName();
    private ProductRepository productRepository;
    private MutableLiveData<ProductName> name = new MutableLiveData<>();

    public void init(ProductRepository productRepository, String name, String brewery) {
        this.productRepository = productRepository;
        this.name.setValue(new ProductName(name, brewery));
    }

    LiveData<HashigoProduct> getProduct() {
        return Transformations.switchMap(name, new Function<ProductName, LiveData<HashigoProduct>>() {
            @Override
            public LiveData<HashigoProduct> apply(ProductName name) {
                return productRepository.getProduct(name.name, name.brewery);
            }
        });
    }
}
