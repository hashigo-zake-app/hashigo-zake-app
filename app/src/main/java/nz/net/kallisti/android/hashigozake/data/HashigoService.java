package nz.net.kallisti.android.hashigozake.data;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;
import nz.net.kallisti.android.hashigozake.data.pies.Pies;
import retrofit2.Call;
import retrofit2.http.GET;

public interface HashigoService {
    @GET("/pies.xml")
    Call<Pies> pieList();

    @GET("/shorttaplist.xml")
    Call<HashigoProducts> tapList();

    @GET("/bottlelist.xml")
    Call<HashigoProducts> bottleList();
}
