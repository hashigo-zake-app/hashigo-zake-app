package nz.net.kallisti.android.hashigozake;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import java.util.List;

import nz.net.kallisti.android.hashigozake.data.PieRepository;
import nz.net.kallisti.android.hashigozake.data.Resource;
import nz.net.kallisti.android.hashigozake.data.pies.Pie;

public class PiesViewModel extends ViewModel {
    private static final String TAG = PiesViewModel.class.getSimpleName();
    private LiveData<Resource<List<Pie>>> availablePies;
    private MediatorLiveData<Resource<List<Pie>>> data = new MediatorLiveData<>();

    private PieRepository pieRepository;

    public void init(PieRepository pieRepository) {
        this.pieRepository = pieRepository;
        refresh();
    }

    public void refresh() {
        if (availablePies != null) {
            data.removeSource(availablePies);
        }
        availablePies = pieRepository.getAvailablePies();
        data.addSource(availablePies, new Observer<Resource<List<Pie>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<Pie>> pies) {
                data.setValue(pies);
            }
        });
    }

    public LiveData<Resource<List<Pie>>> getAvailablePies() {
        return data;
    }
}
