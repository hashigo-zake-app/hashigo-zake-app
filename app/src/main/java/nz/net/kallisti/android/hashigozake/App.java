package nz.net.kallisti.android.hashigozake;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

import nz.net.kallisti.android.hashigozake.data.AppDatabase;
import nz.net.kallisti.android.hashigozake.data.HashigoService;
import nz.net.kallisti.android.hashigozake.data.PieRepository;
import nz.net.kallisti.android.hashigozake.data.ProductRepository;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class App extends Application {
    private static PieRepository pieRepository;
    private static ProductRepository productRepository;

    private static Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE hashigoproduct ADD COLUMN version TEXT");
            database.execSQL("ALTER TABLE pie ADD COLUMN version TEXT");
            database.execSQL("CREATE INDEX index_hashigoproduct_type_version ON hashigoproduct (type, version)");
            database.execSQL("CREATE INDEX index_pie_version ON pie (version)");
            database.execSQL("CREATE INDEX index_pie_available ON pie (available)");
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        final AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "hashigo")
                .addMigrations(MIGRATION_1_2)
                .build();

        final int cacheSize = 10 * 1024 * 1024; // 10 MB
        final Cache cache = new Cache(getCacheDir(), cacheSize);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.hashigozake.co.nz/")
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .client(okHttpClient)
                .build();

        final HashigoService hashigoService = retrofit.create(HashigoService.class);
        pieRepository = new PieRepository(hashigoService, db.pieDao());
        productRepository = new ProductRepository(hashigoService, db.productDao());
    }

    public PieRepository getPieRepository() {
        return pieRepository;
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }
}
