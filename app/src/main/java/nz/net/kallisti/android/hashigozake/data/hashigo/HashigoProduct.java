package nz.net.kallisti.android.hashigozake.data.hashigo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Element;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This represents an individual beer and its details
 *
 * @author robin
 */
@Entity(
        primaryKeys = {"name", "brewery", "type"},
        indices = {
                @Index(value = {"type", "pouringStatus"}),
                @Index(value = {"type", "version"})
        })
public class HashigoProduct implements Serializable {
    private static final long serialVersionUID = 2414225170180565272L;

    public enum PouringStatus {
        NO, NOW, NEXT, NEWEST
    }

    @NonNull
    public String type;
    /**
     * The name of the beer
     */
    @Element(name = "Name", required = false)
    @NonNull
    public String name;

    @Element(name = "Price", required = false)
    public String price;

    @Element(name = "Volume", required = false)
    public String volume;
    /**
     * The servings that it's available in
     */
    @Ignore
    private List<HashigoServing> servings = new ArrayList<>();
    /**
     * The URL of the image
     */
    @Element(name = "Image", required = false)
    public String image;
    /**
     * The alcohol percentage
     */
    @Element(name = "ABV", required = false)
    public String abv;
    /**
     * Is it on handpump?
     */
    @Element(name = "Handpump", required = false)
    public String handpump;
    /**
     * The country it's from
     */
    @Element(name = "Country", required = false)
    public String country;
    /**
     * The brewery it's from
     */
    @Element(name = "Brewery", required = false)
    @NonNull
    public String brewery;
    /**
     * A brief description
     */
    @Element(name = "Description", required = false)
    public String description;

    @Element(name = "QuestId", required = false)
    public float questId;
    /**
     * The volume of the keg. This is usually unpopulated (i.e. is '0.000000')
     */
    @Element(name = "KegVolume", required = false)
    public float kegVolume;
    /**
     * The current pouring status
     */
    @Element(name = "Pouring", required = false)
    public String pouringStatus;
    /**
     * The URL to the ratebeer page for this
     */
    @Element(name = "Ratebeer", required = false)
    public String ratebeerURL;

    @Element(name = "Style", required = false)
    public String style;

    public String version;

    /**
     * Is it on handpump?
     *
     * @return <code>true</code> or <code>false</code>
     */
    public boolean isHandpump() {
        return "Yes".equals(handpump);
    }

    /**
     * Set if it's on handpump
     *
     * @param isHandpump <code>true</code> or <code>false</code>
     */
    public void setHandpump(boolean isHandpump) {
        if (isHandpump) {
            handpump = "Yes";
        } else {
            handpump = "No";
        }
    }

    /**
     * Get the brief description
     *
     * @return the description
     */
    public String getDescription() {
        if (description != null) {
            return description;
        } else {
            return style;
        }
    }

    /**
     * Get the current pouring status
     *
     * @return the pouringStatus
     */
    public PouringStatus getPouringStatus() {
        switch (pouringStatus) {
            case "Now":
                return PouringStatus.NOW;
            case "Next":
                return PouringStatus.NEXT;
            case "NEWEST":
                return PouringStatus.NEWEST;
            default:
                return PouringStatus.NO;
        }
    }

    /**
     * Get a list of the available servings
     *
     * @return the servings
     */
    public List<HashigoServing> getServings() {
        return servings;
    }

    /**
     * Adds a serving
     *
     * @param serving
     */
    public void addServing(HashigoServing serving) {
        servings.add(serving);
    }

    /**
     * Set whether this is the newest product
     */
    public void setNewest() {
        pouringStatus = PouringStatus.NEWEST.toString();
    }

    /**
     * Provides the ratebeer URL
     *
     * @return the ratebeer URL, or <code>null</code> if it doesn't exist.
     */
    public String getRatebeerURL() {
        return ratebeerURL;
    }

    @Override
    public String toString() {
        String res;
        if (brewery != null && brewery.length() > 0) {
            res = brewery + " " + name;
        } else {
            res = name;
        }
        return res;
    }

}
