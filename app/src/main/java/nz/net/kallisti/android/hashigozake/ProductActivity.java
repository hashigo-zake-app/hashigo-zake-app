package nz.net.kallisti.android.hashigozake;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

public class ProductActivity extends AppCompatActivity {
    public static final String TAG = ProductActivity.class.getSimpleName();
    public static final String TITLE = "nz.net.kallisti.android.hashigozake.title";
    public static final String PRODUCT = "nz.net.kallisti.android.hashigozake.product";

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.subtitle)
    TextView subtitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        HashigoProduct product = (HashigoProduct) intent.getSerializableExtra(PRODUCT);

        collapsingToolbar.setTitle(product.name);
        subtitle.setText(product.brewery);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, ProductFragment.newInstance(product))
                .commit();
    }
}
