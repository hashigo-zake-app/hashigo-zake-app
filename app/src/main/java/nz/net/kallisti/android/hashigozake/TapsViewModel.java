package nz.net.kallisti.android.hashigozake;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import java.util.List;

import nz.net.kallisti.android.hashigozake.data.ProductRepository;
import nz.net.kallisti.android.hashigozake.data.Resource;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

public class TapsViewModel extends ViewModel {
    private static final String TAG = TapsViewModel.class.getSimpleName();
    private LiveData<Resource<List<HashigoProduct>>> taps;
    private MediatorLiveData<Resource<List<HashigoProduct>>> data = new MediatorLiveData<>();
    private ProductRepository productRepository;


    public void init(ProductRepository productRepository) {
        this.productRepository = productRepository;
        refresh();
    }

    public void refresh() {
        if (taps != null) {
            data.removeSource(taps);
        }
        taps = productRepository.getTaps();
        data.addSource(taps, new Observer<Resource<List<HashigoProduct>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<HashigoProduct>> taps) {
                data.setValue(taps);
            }
        });
    }

    public LiveData<Resource<List<HashigoProduct>>> getTaps() {
        return data;
    }
}
