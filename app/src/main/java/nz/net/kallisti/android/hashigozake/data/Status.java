package nz.net.kallisti.android.hashigozake.data;

public enum Status {
    SUCCESS, ERROR, LOADING
}
