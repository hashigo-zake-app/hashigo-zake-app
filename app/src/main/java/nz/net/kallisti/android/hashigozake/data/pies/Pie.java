/*	Copyright (C) 2013 Robin Sheat

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nz.net.kallisti.android.hashigozake.data.pies;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Element;

/**
 * This represents a single pie.
 *
 * @author Robin Sheat
 */
@Entity(
        indices = {
                @Index(value = "available"),
                @Index(value = "version")
        })
public class Pie {
    @PrimaryKey
    @Element(name = "Name", required = false)
    @NonNull
    private String name;

    @Element(name = "Available", required = false)
    private String available;

    public String version;

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public boolean isAvailable() {
        return available.equals("Yes");
    }
}
