package nz.net.kallisti.android.hashigozake.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.pies.Pie;

@Database(entities = {Pie.class, HashigoProduct.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PieDao pieDao();

    public abstract ProductDao productDao();
}
