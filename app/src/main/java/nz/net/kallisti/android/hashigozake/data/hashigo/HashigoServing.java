package nz.net.kallisti.android.hashigozake.data.hashigo;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * This describes a serving by volume and cost
 *
 * @author robin
 */
public class HashigoServing implements Serializable {
    private static final long serialVersionUID = 5468605474604198486L;

    @NonNull
    public String type;

    @NonNull
    private String volume;

    private int price; // in cents

    private int takeawayPrice;

    /**
     * Create a new serving with the provided volume and price
     *
     * @param volume the volume of the serving
     * @param price  the price of the serving (in cents)
     */
    public HashigoServing(String type, String volume, int price) {
        this.type = type;
        this.volume = volume;
        this.price = price;
    }

    /**
     * Gets the volume of this serving
     *
     * @return the volume in ml
     */
    public String getVolume() {
        return volume;
    }

    /**
     * Gets the price of this serving
     *
     * @return the price of this serving, in cents
     */
    public int getPrice(boolean isSobaMember) {
        if (isSobaMember) {
            return (int) (price * 0.9);
        }

        return price;
    }

    public void setTakeawayPrice(int takeawayPrice) {
        this.takeawayPrice = takeawayPrice;
    }

    public int getTakeawayPrice() {
        return takeawayPrice;
    }

}
