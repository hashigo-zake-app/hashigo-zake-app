package nz.net.kallisti.android.hashigozake;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MainPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = MainPagerAdapter.class.getSimpleName();

    private final Context context;

    public MainPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = BeerListFragment.newInstance();
                break;
            case 1:
                fragment = BottleListFragment.newInstance();
                break;
            case 2:
                fragment = PieListFragment.newInstance();
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.title_activity_beer_list);
            case 1:
                return context.getString(R.string.title_activity_bottle_list);
            case 2:
                return context.getString(R.string.title_activity_pie_list);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}