package nz.net.kallisti.android.hashigozake;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.ProductRepository;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.ui.ProductAdapter;
import nz.net.kallisti.android.hashigozake.ui.ProductListAdapter;


public class ProductFragment extends Fragment implements ProductAdapter.ProductLinkClickListener {
    private static final String TAG = ProductFragment.class.getSimpleName();
    private static final String ARG_PRODUCT_NAME = "product_name";
    private static final String ARG_PRODUCT_BREWERY = "product_brewery";

    @BindView(R.id.list)
    RecyclerView list;

    private ProductAdapter adapter;
    private ProductViewModel viewModel;

    public static ProductFragment newInstance(HashigoProduct product) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODUCT_NAME, product.name);
        args.putString(ARG_PRODUCT_BREWERY, product.brewery);
        fragment.setArguments(args);
        return fragment;
    }

    public ProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ProductAdapter(this);

        if (getArguments() == null) {
            return;
        }

        String name = getArguments().getString(ARG_PRODUCT_NAME);
        String brewery = getArguments().getString(ARG_PRODUCT_BREWERY);
        final ProductRepository productRepository = ((App) getActivity().getApplication()).getProductRepository();
        viewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        viewModel.init(productRepository, name, brewery);
        viewModel.getProduct().observe(this, new Observer<HashigoProduct>() {
            @Override
            public void onChanged(@Nullable HashigoProduct product) {
                adapter.setProduct(product);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ProductAdapter.ITEM_LINKS:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        list.setLayoutManager(manager);
        list.setAdapter(adapter);
    }

    @Override
    public void onProductLinkClick(ProductAdapter.LinkType type, HashigoProduct product) {
        Intent intent;
        switch (type) {
            case UNTAPPD:
                intent = new Intent(Intent.ACTION_VIEW, BeerListUtils.getUntappdUri(getActivity(), product));
                startActivity(intent);
                break;
            case RATEBEER:
                intent = new Intent(Intent.ACTION_VIEW, BeerListUtils.getRatebeerUri(getActivity(), product));
                startActivity(intent);
                break;
        }
    }
}
