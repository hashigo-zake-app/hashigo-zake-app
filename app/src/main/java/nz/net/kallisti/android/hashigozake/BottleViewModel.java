package nz.net.kallisti.android.hashigozake;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import java.util.List;

import nz.net.kallisti.android.hashigozake.data.ProductRepository;
import nz.net.kallisti.android.hashigozake.data.Resource;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

public class BottleViewModel extends ViewModel {
    private static final String TAG = BottleViewModel.class.getSimpleName();
    private ProductRepository productRepository;
    private LiveData<Resource<List<HashigoProduct>>> bottles;
    private MediatorLiveData<Resource<List<HashigoProduct>>> data = new MediatorLiveData<>();

    public void init(ProductRepository productRepository) {
        this.productRepository = productRepository;
        refresh();
    }

    public void refresh() {
        if (bottles != null) {
            data.removeSource(bottles);
        }
        bottles = productRepository.getBottles();
        data.addSource(bottles, new Observer<Resource<List<HashigoProduct>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<HashigoProduct>> products) {
                data.setValue(products);
            }
        });
    }

    public LiveData<Resource<List<HashigoProduct>>> getBottles() {
        return data;
    }
}
