package nz.net.kallisti.android.hashigozake.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import nz.net.kallisti.android.hashigozake.data.pies.Pie;
import nz.net.kallisti.android.hashigozake.data.pies.Pies;
import retrofit2.Call;

public class PieRepository {
    private static final String TAG = PieRepository.class.getSimpleName();

    private final HashigoService hashigoService;
    private final PieDao pieDao;

    public PieRepository(HashigoService hashigoService, PieDao pieDao) {
        this.hashigoService = hashigoService;
        this.pieDao = pieDao;
    }

    public LiveData<Resource<List<Pie>>> getAvailablePies() {
        return new NetworkBoundResource<List<Pie>, Pies>() {
            @Override
            protected void saveCallResult(Pies body) {
                pieDao.insertLatest(body.getPies());
            }

            @NonNull
            @Override
            protected LiveData<List<Pie>> loadFromDb() {
                return pieDao.getAvailable();
            }

            @NonNull
            @Override
            protected Call<Pies> createCall() {
                return hashigoService.pieList();
            }
        }.getAsLiveData();
    }
}
