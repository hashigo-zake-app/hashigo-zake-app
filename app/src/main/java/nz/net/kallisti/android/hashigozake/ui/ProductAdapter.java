package nz.net.kallisti.android.hashigozake.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.BeerListUtils;
import nz.net.kallisti.android.hashigozake.R;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoServing;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    public static final int ITEMS = 3;
    public static final int ITEM_INFO = 0;
    public static final int ITEM_PRICE = 1;
    public static final int ITEM_LINKS = 2;
    private static final String TAG = ProductAdapter.class.getSimpleName();

    private ProductLinkClickListener listener;
    private HashigoProduct product;

    public ProductAdapter(ProductLinkClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case ITEM_INFO:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_info, parent, false);
                return new InfoViewHolder(v);
            case ITEM_PRICE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_price, parent, false);
                return new PriceViewHolder((ViewGroup) v);
            case ITEM_LINKS:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_links, parent, false);
                return new LinksViewHolder(v, listener);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(product);
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ITEM_INFO;
            case 1:
                return ITEM_PRICE;
            default:
                return ITEM_LINKS;
        }
    }

    @Override
    public int getItemCount() {
        if (product == null) {
            return 0;
        }
        return ITEMS;
    }

    public void setProduct(HashigoProduct product) {
        Log.d(TAG, "setProduct");
        this.product = product;
        notifyDataSetChanged();
    }

    static class InfoViewHolder extends ViewHolder {
        @BindView(R.id.abv)
        TextView abv;
        @BindView(R.id.country)
        TextView country;
        @BindView(R.id.description)
        TextView description;

        public InfoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindView(HashigoProduct product) {
            abv.setText(product.abv);
            country.setText(product.country);
            description.setText(product.getDescription());
        }
    }

    static class PriceViewHolder extends ViewHolder {
        public PriceViewHolder(ViewGroup itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bindView(HashigoProduct product) {
            ViewGroup pricesContainer = (ViewGroup) itemView;
            LayoutInflater inflater = (LayoutInflater) itemView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            for (HashigoServing serving : product.getServings()) {
                View servingView = inflater.inflate(R.layout.beer_detail_serving, pricesContainer, false);
                TextView price = (TextView) servingView.findViewById(R.id.beer_detail_serving_price);
                TextView volume = (TextView) servingView.findViewById(R.id.beer_detail_serving_volume);

                price.setText(BeerListUtils.formatPrice(serving.getPrice(false)));
                volume.setText(serving.getVolume());

                pricesContainer.addView(servingView);
            }
        }
    }

    static class LinksViewHolder extends ViewHolder {
        private final ProductLinkClickListener listener;
        @BindView(R.id.ratebeer_btn)
        Button ratebeerBtn;
        @BindView(R.id.untappd_btn)
        Button untappdBtn;

        public LinksViewHolder(View itemView, ProductLinkClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.listener = listener;
        }

        @Override
        public void bindView(final HashigoProduct product) {
            ratebeerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProductLinkClick(LinkType.RATEBEER, product);
                }
            });
            untappdBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProductLinkClick(LinkType.UNTAPPD, product);
                }
            });
        }
    }

    public enum LinkType {RATEBEER, UNTAPPD}

    public interface ProductLinkClickListener {
        void onProductLinkClick(LinkType type, HashigoProduct product);
    }

    static abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void bindView(final HashigoProduct product);
    }
}
