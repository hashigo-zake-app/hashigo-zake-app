package nz.net.kallisti.android.hashigozake.data;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoServing;
import retrofit2.Call;

public class ProductRepository {
    private static final String TAG = ProductRepository.class.getSimpleName();

    private HashigoService hashigoService;
    private ProductDao productDao;

    public ProductRepository(HashigoService hashigoService, ProductDao productDao) {
        this.hashigoService = hashigoService;
        this.productDao = productDao;
    }

    public LiveData<Resource<List<HashigoProduct>>> getTaps() {
        return new NetworkBoundResource<List<HashigoProduct>, HashigoProducts>() {
            @Override
            protected void saveCallResult(HashigoProducts body) {
                List<HashigoProduct> products = new ArrayList<>();
                for (HashigoProduct product : body.getProducts()) {
                    if (product.name == null || product.brewery == null) {
                        continue;
                    }
                    if (body.isLatestProduct(product)) {
                        product.setNewest();
                    }
                    products.add(product);
                }
                productDao.insertLatestByType("tap", products);
            }

            @NonNull
            @Override
            protected LiveData<List<HashigoProduct>> loadFromDb() {
                return Transformations.map(productDao.getTaps(), new Function<List<HashigoProduct>, List<HashigoProduct>>() {
                    @Override
                    public List<HashigoProduct> apply(List<HashigoProduct> products) {
                        return prepareServings(products);
                    }
                });
            }

            @NonNull
            @Override
            protected Call<HashigoProducts> createCall() {
                return hashigoService.tapList();
            }
        }.getAsLiveData();
    }


    public LiveData<Resource<List<HashigoProduct>>> getBottles() {
        return new NetworkBoundResource<List<HashigoProduct>, HashigoProducts>() {
            @Override
            protected void saveCallResult(HashigoProducts body) {
                List<HashigoProduct> products = new ArrayList<>();
                for (HashigoProduct product : body.getProducts()) {
                    if (product.name == null || product.brewery == null) {
                        continue;
                    }
                    products.add(product);
                }
                productDao.insertLatestByType("bottle", products);
            }

            @NonNull
            @Override
            protected LiveData<List<HashigoProduct>> loadFromDb() {
                return Transformations.map(productDao.getBottles(), new Function<List<HashigoProduct>, List<HashigoProduct>>() {
                    @Override
                    public List<HashigoProduct> apply(List<HashigoProduct> products) {
                        return prepareServings(products);
                    }
                });
            }

            @NonNull
            @Override
            protected Call<HashigoProducts> createCall() {
                return hashigoService.bottleList();
            }
        }.getAsLiveData();
    }

    public LiveData<HashigoProduct> getProduct(String name, String brewery) {
        return Transformations.map(productDao.getProduct(name, brewery), new Function<List<HashigoProduct>, HashigoProduct>() {
            @Override
            public HashigoProduct apply(List<HashigoProduct> products) {
                products = prepareServings(products);
                if (products.size() == 0) {
                    return null;
                }
                HashigoProduct product = products.get(0);
                for (HashigoProduct p : products.subList(1, products.size())) {
                    for (HashigoServing serving : p.getServings()) {
                        product.addServing(serving);
                    }
                }
                return product;
            }
        });
    }

    private List<HashigoProduct> prepareServings(List<HashigoProduct> products) {
        for (HashigoProduct product : products) {
            if (product.price == null || product.volume == null) {
                Log.d(TAG, "prepareAPIProducts: price or volume missing");
                continue;
            }
            String[] prices = product.price.split("/");
            String[] volumes = product.volume.split("/");
            if (prices.length != volumes.length) {
                Log.d(TAG, "prepareAPIProducts: price/volume length mismatch");
                continue;
            }
            for (int i = 0; i < prices.length; i++) {
                try {
                    String[] price = prices[i].split("~");
                    product.addServing(new HashigoServing(
                            product.type,
                            volumes[i],
                            Math.round(Float.parseFloat(price[0].replace("$", "")) * 100f)
                    ));
                    // TODO: takeaway price
                } catch (NumberFormatException ignored) {
                }
            }
        }
        return products;
    }
}
