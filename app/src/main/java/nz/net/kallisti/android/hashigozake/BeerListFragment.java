package nz.net.kallisti.android.hashigozake;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import net.peterkuterna.android.apps.pinnedheader.PinnedListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.ProductRepository;
import nz.net.kallisti.android.hashigozake.data.Resource;
import nz.net.kallisti.android.hashigozake.data.Status;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.ui.ProductListAdapter;


public class BeerListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {
    public static final String TAG = BeerListFragment.class.getSimpleName();

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.list)
    PinnedListView list;

    private ProductListAdapter adapter;
    private TapsViewModel viewModel;

    public static BeerListFragment newInstance() {
        return new BeerListFragment();
    }

    public BeerListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        adapter = new ProductListAdapter(getActivity());
        final ProductRepository productRepository = ((App) getActivity().getApplication()).getProductRepository();
        viewModel = ViewModelProviders.of(this).get(TapsViewModel.class);
        viewModel.init(productRepository);
        viewModel.getTaps().observe(this, new Observer<Resource<List<HashigoProduct>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<HashigoProduct>> hashigoProducts) {
                if (hashigoProducts == null) {
                    return;
                }
                if (hashigoProducts.status == Status.LOADING) {
                    if (!refreshLayout.isRefreshing()) {
                        progress.setVisibility(View.VISIBLE);
                    }
                } else {
                    progress.setVisibility(View.GONE);
                    refreshLayout.setRefreshing(false);
                }
                if (hashigoProducts.data != null) {
                    adapter.updateAll(hashigoProducts.data);
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_beer_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        list.setAdapter(adapter);
        list.setOnScrollListener(adapter);
        list.setPinnedHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.beer_list_item_header, list, false));
        list.setOnItemClickListener(this);
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                onRefresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        viewModel.refresh();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final HashigoProduct product = (HashigoProduct) adapter.getItem(position);
        Intent intent = new Intent(getActivity(), ProductActivity.class);
        intent.putExtra(ProductActivity.TITLE, "Beer");
        intent.putExtra(ProductActivity.PRODUCT, product);
        startActivity(intent);
    }
}
